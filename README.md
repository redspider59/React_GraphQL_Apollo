**LIVE PRODUCTION ENVIRONMENT AT: https://dry-reaches-71828.herokuapp.com/**

**npm install to install al required modules**

`###1)GraphQL*`:
GraphQL is a query language for APIs and a runtime for fulfilling those queries with your existing data.

`###2)Schema`:
Schema looks for the root Query which is required to define the API layout and its response.

`###3)Concurrently`:
 Only used to run multiple commands at once, Here will be used to run fron-end and back-end server at once.Define in package.json:
        1)command client: npm start --prefix client
        2)dev command :concurrently \"npm run server\" \"npm run client\"

`###Apollo Client`
Apollo Client is the best way to use GraphQL to build client applications. The client is designed to help you quickly build a UI that fetches data with GraphQL, and can be used with any JavaScript front-end.
Visit : 'https://www.apollographql.com/docs/react/'

###`CORS`
Cross-Origin Resource Sharing (CORS) is a mechanism that uses additional HTTP headers to tell a browser to let a web application running at one origin (domain) have permission to access selected resources from a server at a different origin.
